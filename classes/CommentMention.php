<?php

namespace StudioBosco\BackendComments\Classes;

class CommentMention
{
    public $comment = null;
    public $user = null;
    public $excerpt = null;
    public $url = null;

    public function __construct(array $attributes = [])
    {
        foreach($attributes as $name => $attribute) {
            if (property_exists($this, $name)) {
                $this->$name = $attribute;
            }
        }
    }

    public function toArray()
    {
        return [
            'comment_id' => $this->comment ? $this->comment->id : null,
            'user_id' => $this->user ? $this->user->id : null,
            'excerpt' => $this->excerpt,
            'url' => $this->url,
        ];
    }
}
