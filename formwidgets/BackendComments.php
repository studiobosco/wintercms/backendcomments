<?php namespace StudioBosco\BackendComments\FormWidgets;

use Input;
use Backend\Classes\FormWidgetBase;
use Backend\Models\User;
use StudioBosco\BackendComments\Models\Comment;

/**
 * backendcomments Form Widget
 */
class BackendComments extends FormWidgetBase
{
    use \Backend\Traits\FormModelWidget;

    protected static $allowedTags = 'br,p,strong,a,i,b,h1,h2,h3,h4,h5,h6,ul,ol,li,span,div';

    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'backendcomments';

    /**
     * Name of the comments relation
     *
     * @var string
     */
    public $relation = 'comments';

    /**
     * Date format
     *
     * @var string
     */
    public $dateFormat = 'Y/m/d - H:i:s';

    /**
     * User model class
     *
     * @var string
     */
    public $userModel = 'Backend\Models\User';

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->fillFromConfig([
            'relation',
            'dateFormat',
        ]);

        $this->valueFrom = $this->relation;
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('backendcomments');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $this->vars['name'] = $this->formField->getName();
        $this->vars['comments'] = $this->getComments();
        $this->vars['model'] = $this->model;
    }

    /**
     * @inheritDoc
     */
    public function loadAssets()
    {
        $this->addCss('css/backendcomments.css', 'StudioBosco.BackendComments');
        $this->addJs('js/backendcomments.js', [
            'build' => 'StudioBosco.BackendComments',
            'type' => 'module',
        ]);
    }

    protected function getComments()
    {
        $relationObject = $this->getRelationObject();
        return $relationObject->get();
    }

    protected function getUserName($userId)
    {
        $userModelClass = $this->userModel;
        $user = $userModelClass::find($userId);

        return $userModelClass ? $user->first_name . ' ' . $user->last_name : '';
    }

    protected function getInput()
    {
        $data = Input::all();
        $path = str_replace('[', '.', $this->formField->getName());
        $path = str_replace(']', '', $path);
        return array_get($data, $path);
    }

    public function getMentionableUsers()
    {
        return User::orderBy('login', 'asc')->get()->pluck('login')->toArray();
    }

    public function onWriteComment()
    {
        $data = $this->getInput();
        $body = trim(strip_tags(array_get($data, 'new_comment.body', ''), self::$allowedTags));

        if (!strlen($body)) {
            return;
        }

        Comment::create([
            'body' => $body,
            'commentable_id' => $this->model->id,
            'commentable_type' => get_class($this->model),
        ]);

        $target = '#' . $this->getId('comments');
        $writeTarget = '#' . $this->getId('write-comment');

        return [
            $target => $this->makePartial('comments', ['comments' => $this->getComments()]),
            $writeTarget => $this->makePartial('write_comment'),
        ];
    }

    public function onDeleteComment()
    {
        $commentId = intval(Input::get('comment_id') ?? '');

        if (!$commentId) {
            return;
        }

        Comment::destroy($commentId);

        $target = '#' . $this->getId('comments');

        return [
            $target => $this->makePartial('comments', [
                'comments' => $this->getComments(),
            ]),
        ];
    }

    public function onEditComment()
    {
        $commentId = intval(Input::get('comment_id') ?? '');

        if (!$commentId) {
            return;
        }

        $comment = Comment::findOrFail($commentId);
        $target = '#' . $this->getId('comment-' . $commentId);

        return [
            $target => $this->makePartial('comment', [
                'comment' => $comment,
                'editing' => true,
            ]),
        ];
    }

    public function onCancelEditComment()
    {
        $commentId = intval(Input::get('comment_id') ?? '');

        if (!$commentId) {
            return;
        }

        $comment = Comment::findOrFail($commentId);
        $target = '#' . $this->getId('comment-' . $commentId);

        return [
            $target => $this->makePartial('comment', [
                'comment' => $comment,
                'editing' => false,
            ]),
        ];
    }

    public function onUpdateComment()
    {
        $commentId = intval(Input::get('comment_id') ?? '');

        if (!$commentId) {
            return;
        }

        $data = $this->getInput();
        $body = trim(strip_tags(array_get($data, 'comment.' . $commentId . '.body', ''), self::$allowedTags));

        $comment = Comment::findOrFail($commentId);
        $comment->update([
            'body' => $body,
        ]);

        $target = '#' . $this->getId('comment-' . $commentId);

        return [
            $target => $this->makePartial('comment', [
                'comment' => $comment,
                'editing' => false,
            ]),
        ];
    }

    public function onToggleCommentReaction()
    {
        $commentId = intval(Input::get('comment_id') ?? '');
        $emoji = Input::get('emoji') ?? '';

        if (!$commentId || !$emoji) {
            return;
        }

        $comment = Comment::findOrFail($commentId);
        $comment->toggleReaction($emoji);

        $target = '#' . $this->getId('comment-' . $commentId);

        return [
            $target => $this->makePartial('comment', [
                'comment' => $comment,
                'editing' => false,
            ]),
        ];
    }
}
