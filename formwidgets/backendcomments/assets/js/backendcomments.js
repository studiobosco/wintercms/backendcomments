import emojis from './utils/emojis.js';
import getCaretOffset from './utils/getCaretOffset.js';
import getOffsetTo from './utils/getOffsetTo.js';

const modifierKeys = ['Alt', 'AltGraph', 'CapsLock', 'Control', 'Fn', 'FnLock', 'Hyper', 'Meta', 'NumLock', 'ScrollLock', 'Shift', 'Super', 'Symbol', 'SymbolLock'];

let state = {
    q: '',
    list: null,
    commentId: null,
    requestHandler: null,
    textarea: null,
    isSelectingMention: false,
};

function initReactionSelectForComment(commentId, requestHandler, additionalRequestData = null) {
    const el = document.getElementById(`comment-reaction-select-${commentId}`);
    const list = el.querySelector('*[data-role="emoji-list"]');
    const searchInput = el.querySelector('input[data-role="search"]');

    state.list = list;
    state.commentId = commentId;
    state.requestHandler = requestHandler;
    state.additionalRequestData = additionalRequestData;
    list.innerHTML = renderEmojiList();
    searchInput.addEventListener('input', handleEmojiSearchInput);
    updateEmojisList();
}
function removeReactionSelectForComment(commentId) {
    $(`#comment-reaction-select-${commentId}`).trigger('close.oc.popover');
}

function initMentionSelectForComment(textarea) {
    state.textarea = textarea;
    let cursorEl = document.getElementById('comment-mention-select-cursor');

    if (!cursorEl) {
        cursorEl = document.createElement('button');
        cursorEl.id = 'comment-mention-select-cursor';
        cursorEl.classList.add('comment-mention-select-cursor');
        cursorEl.setAttribute('data-control', 'popover');
        cursorEl.setAttribute('data-placement', 'top');
        cursorEl.setAttribute('data-content-from', '#comment-mention-select-template');
        document.body.appendChild(cursorEl);
    }

    textarea.removeEventListener('input', handleTextareaInput);
    textarea.addEventListener('input', handleTextareaInput);
    textarea.removeEventListener('blur', handleTextareaBlur);
    textarea.addEventListener('blur', handleTextareaBlur);
    $(cursorEl).off('hide.oc.popover', handleMentionPopoverHide);
    $(cursorEl).on('hide.oc.popover', handleMentionPopoverHide);
}

function handleTextareaInput(event) {
    const cursorEl = document.getElementById('comment-mention-select-cursor');
    const textarea = state.textarea;
    const lastChar = textarea.value.substring(textarea.selectionStart, textarea.selectionStart - 1);

    if (state.isSelectingMention) {
        const search = textarea.value.substring(state.cursorPosition + 1, textarea.selectionStart);

        updateMentionSelectList(search);

        if (!hasMatchingMentions(search)) {
            if (cursorEl) {
                $(cursorEl).ocPopover('hide');
            }
        }
    } else if (lastChar === '@') {
        state.cursorPosition = textarea.selectionStart;
        const textareaOffset = getOffsetTo(textarea, document.body);
        cursorEl.style.left = (textareaOffset.x - 16) + 'px';
        cursorEl.style.top = (textareaOffset.y + 6) + 'px';
        cursorEl.click();

        state.isSelectingMention = true;
    }

    return;
}

function updateMentionSelectList(search = null) {
    Array.from(document.getElementById('comment-mention-user-list').querySelectorAll('*[data-user]'))
    .filter((btn) => {
        if (!search || btn.getAttribute('data-user').toLowerCase().indexOf(search.toLowerCase().trim()) !== -1) {
            btn.parentElement.classList.remove('hidden');
        } else {
            btn.parentElement.classList.add('hidden');
        }
    });
}

function hasMatchingMentions(search) {
    return Array.from(document.getElementById('comment-mention-user-list').querySelectorAll('*[data-user]'))
    .filter((btn) => {
        return btn.getAttribute('data-user').toLowerCase().indexOf(search.toLowerCase()) !== -1;
    }).length > 0;
}

function handleTextareaBlur() {
    const textarea = state.textarea;
    const cursorEl = document.getElementById('comment-mention-select-cursor');

    if (state.isSelectingMention) {
        return;
    }

    $(cursorEl).ocPopover('hide');
    textarea.removeEventListener('input', handleTextareaInput);
    textarea.removeEventListener('blur', handleTextareaBlur);
    $(cursorEl).off('hide.oc.popover', handleMentionPopoverHide);
    state.textarea = null;
    state.isSelectingMention = false;
}

function handleMentionPopoverHide() {
    state.isSelectingMention = false;
}

function addMention(username) {
    const textarea = state.textarea;
    const cursorEl = document.getElementById('comment-mention-select-cursor');
    if (cursorEl) {
        $(cursorEl).ocPopover('hide');
    }
    if (textarea) {
        const pos = state.cursorPosition;
        textarea.value = textarea.value.substring(0, pos) + username + textarea.value.substring(pos + username.length) + ' ';
        textarea.selectionStart = textarea.selectionStart + username.length + 1;
        textarea.focus();
    }
}

window.initReactionSelectForComment = initReactionSelectForComment;
window.removeReactionSelectForComment = removeReactionSelectForComment;
window.initMentionSelectForComment = initMentionSelectForComment;
window.addMention = addMention;

function renderEmojiList() {
    let html = '';

    for(let group in emojis) {
        html += `
        <li data-group="${group}" class="hidden">
            <h4 class="m-b">${group}</h4>
            <div class="comment-reactions-select-emojis">
        `;

        for(let key in emojis[group]) {
            html += `
                <button
                    data-emoji="${key}"
                    class="hidden"
                    data-request="${state.requestHandler}"
                    data-request-data="comment_id: ${state.commentId}, emoji: '${emojis[group][key]}'${state.additionalRequestData ? ', ' + state.additionalRequestData : ''}"
                    onclick="removeReactionSelectForComment(${state.commentId})"
                    type="button"
                >
                    ${emojis[group][key]}
                </button>
            `;
        }

        html += `
            </div>
        </li>
        `;
    }
    return html;
}

function handleEmojiSearchInput(event) {
    state.q = event.target.value;

    updateEmojisList();
}

function getMatchingEmojis() {
    const q = state.q.trim().toLowerCase();

    if (!q.length) {
        return emojis;
    }

    const _emojis = {};

    for(let group in emojis) {
        for(let key in emojis[group]) {
            if (key.indexOf(q) !== -1) {
            if (!_emojis[group]) {
                _emojis[group] = {};
            }

            _emojis[group][key] = emojis[group][key];
            }
        }
    }

    return _emojis;
}

function updateEmojisList(list) {
    const _emojis = getMatchingEmojis();

    Array.from(state.list.querySelectorAll('*[data-group]'))
    .forEach((el) => {
        el.classList.add('hidden');
    });
    Array.from(state.list.querySelectorAll('*[data-emoji]'))
    .forEach((el) => {
        el.classList.add('hidden');
    });

    for(let group in _emojis) {
        const el = state.list.querySelector(`*[data-group="${group}"]`);

        if (el) {
            el.classList.remove('hidden');
        }

        for(let key in _emojis[group]) {
            const el = state.list.querySelector(`*[data-emoji="${key}"]`);

            if (el) {
                el.classList.remove('hidden');
            }
        }
    }
}
