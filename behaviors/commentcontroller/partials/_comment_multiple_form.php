<div id="Comments-sendpopup">
    <?= Form::ajax('onCommentMultipleForm', [
        'data-popup-load-indicator' => true,
        'sessionKey' => $newSessionKey,
    ]) ?>

        <!-- Passable fields -->
        <input type="hidden" name="record_ids" value="<?= implode(',', $recordIds) ?>" />

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="popup">&times;</button>
            <h4 class="modal-title">
                <?= e(trans('studiobosco.backendcomments::lang.comment_multiple')) ?>
            </h4>
        </div>

        <div class="modal-body">
        <p class="m-b">
            <?= e(trans('studiobosco.backendcomments::lang.comment_multiple_confirm', ['count' => count($recordIds)])); ?>
        </p>

        <div class="form-group">
            <label for="Comments-body"><?= e(trans('studiobosco.backendcomments::lang.body')) ?></label>
            <textarea id="Comments-body" class="form-control" name="Comment[body]" required></textarea>
        </div>

        <div class="modal-footer">
            <?php if ($this->readOnly): ?>
                <button
                    type="button"
                    class="btn btn-default"
                    data-dismiss="popup">
                    <?= e(trans('backend::lang.relation.close')) ?>
                </button>
            <?php else: ?>
                <button
                    class="btn btn-primary"
                    type="submit"
                >
                    <?= e(trans('studiobosco.backendcomments::lang.add_comment')) ?>
                </button>
                <button
                    type="button"
                    class="btn btn-default"
                    data-dismiss="popup">
                    <?= e(trans('backend::lang.relation.cancel')) ?>
                </button>
            <?php endif ?>
        </div>
    <?= Form::close() ?>
</div>
